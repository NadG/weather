
weatherApp.controller('forecastCtrl', ['$scope', '$routeParams','citySer', 'weatherSer', '$timeout', '$q',function($scope, $routeParams, citySer, weatherSer, $timeout, $q) {
    $scope.city = citySer.city;
   
    //RISPOSTA 5 ORE
    $scope.night;
    $scope.day = true;
    function promise() {
        $scope.weatherResult = weatherSer.GetWeather($scope.city);
        var resultObj = $scope.weatherResult; 
        var deferred = $q.defer();
        $timeout(function() {
            if (resultObj !== undefined) {
                //determineNightTime();
                for (var i = 0, l = resultObj.list.length; i < l; i++) {
                    var obj = resultObj.list[i];
                    var dateTxt = resultObj.list[i].dt_txt;
                    var hour = dateTxt.slice(11, 19);
                    var a = hour.split(':');
                    var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
                    //is night
                    /*$scope.night = 0 <= seconds <= 18000  || seconds >= 68400 ? true : false;
                    console.log(hour, seconds + ' is night: ' + $scope.night);*/
                    //is day
                    $scope.day = seconds > 18000 && seconds < 68400 ? true : false;
                    console.log(hour, seconds + ' is day: ' + $scope.day);
                }
                console.log(resultObj, 'done');
                deferred.resolve();
            } else {
                console.log('fail');
                deferred.reject();
            }
        }, 1000);
        return deferred.promise;
    }

   /* function determineNightTime (resultObj) {
        for (var i = 0, l = resultObj.list.length; i < l; i++) {
            var obj = resultObj.list[i];
            var dateTxt = resultObj.list[i].dt_txt;
            var hour = dateTxt.slice(11, 19);
            console.log(hour);
            var a = hour.split(':');
            var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
        }
    } */
    //RISPOSTA CURRENT TIME
    $scope.currWeatherResult = weatherSer.GetCurrWeather($scope.city);
    console.log($scope.currWeatherResult);

    //°C
    $scope.convertToCelsius = function(K){
        return Math.round(K - 273);
    }
    // DATA LEGGIBILE
    $scope.convertDate = function(dt) {
        return new Date(dt *1000);//moment().format("Do MMMM, kk:mm");//
    }

    promise();

}]);
