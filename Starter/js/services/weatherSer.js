weatherApp.service('weatherSer', ['$resource',function($resource){
    // gg diversi 
    //var apiWeather = 'http://api.openweathermap.org/data/2.5/forecast/daily?&APPID=5f2eeda79872e88b7ace3eff2a59889b';
    
    // ORARI     
    var apiWeather = 'http://api.openweathermap.org/data/2.5/forecast?&APPID=5f2eeda79872e88b7ace3eff2a59889b';
    //current time
    var currWeather = 'http://api.openweathermap.org/data/2.5/weather?&APPID=5f2eeda79872e88b7ace3eff2a59889b';

    this.GetWeather = function(city, days){
        var weatherApi = $resource(apiWeather, {callback: "JSON_CALLBACK"}, {get: {method: "JSONP"}});

        return weatherApi.get({
                q:          city,
                cnt:        5 //h or days
            });
    }
 

    this.GetCurrWeather = function(city){
        var weatherApi = $resource(currWeather, {callback: "JSON_CALLBACK"}, {get: {method: "JSONP"}});

        return weatherApi.get({
                q:          city
            });
    }
    //JSONP per richieste cross origin
    //$scope non è utilizzabile qui in quanto appartiene al controller
    //this.GetWeather === $scope.getWeather nei servizi
    //service: reusable business logic independent of views
}]);

weatherApp.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});
