// MODULE
var weatherApp = angular.module('weatherApp', ['ngRoute', 'ngResource']);
//ngRoute e Resource vanno caricati da cdn
// ROUTES
weatherApp.config(function ($routeProvider) {
   
    $routeProvider
    
    .when('/home', {
        templateUrl: 'views/homePage.html',
        controller: 'homeCtrl'
    })
    .when('/forecast', {
        templateUrl: 'views/forecastPage.html',
        controller: 'forecastCtrl'
    });
});
