weatherApp.controller('homeCtrl', ['$scope', 'citySer', '$location','$q','$timeout', function($scope, citySer, $location, $q, $timeout) {
    
    $scope.city = citySer.city;

    $scope.$watch('city', function (){
    	citySer.city = $scope.city;
    });

    $scope.goto = function() {
    	swal({
		  	title: '<h1 style="font-family: Raleway; color: #05a2f0;">Retreiving forecast</h1>',
		  	text: 'Just wait a little bit',
		  	showConfirmButton: false,
		  	timer: 3000,
		  	type: 'info'
		}).then(
		  function () {},
		  // handling the promise rejection
		  function (dismiss) {
		    if (dismiss === 'timer') {
		    	swal({
				  	title: '<h1 style="font-family: Raleway; color:#b7d545;">Forecast retreived!</h1>',
				  	showConfirmButton: false,
				  	type: 'success',
				  	timer: 1500
				})
		    }
		  }
		);
    	$timeout(gotoAtTimeout, 3000);
	}

    function gotoAtTimeout() {
	    $location.path("/forecast");
	}

}]);

/*
var testObject = { 'one': 1, 'two': 2, 'three': 3 };

// Put the object into storage
localStorage.setItem('testObject', JSON.stringify(testObject));

// Retrieve the object from storage
var retrievedObject = localStorage.getItem('testObject');

console.log('retrievedObject: ', JSON.parse(retrievedObject));
*/